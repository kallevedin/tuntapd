package tuntapd

import (
	"errors"
	"syscall"
	"unsafe"
)

// #include <stdlib.h>
// #include <string.h>
// #include "./interface.h"
import "C"


// Creates a tap/tun interface
func createInterface(interfaceType, interfaceName string, user uint32, group uint32) (string, error) {

	var cIfType C.int
	if interfaceType == "tun" {
		cIfType = syscall.IFF_TUN | syscall.IFF_NO_PI
	} else if interfaceType == "tap" {
		cIfType = syscall.IFF_TAP | syscall.IFF_NO_PI
	} else {
		return "", errors.New("Unknown device type " + interfaceType)
	}

	// Calls create_interface() in interface.c
	// Read the CGO manual if you want to understand this. Its quite short and simple actually.
	cIfName := C.CString(interfaceName)
	defer C.free(unsafe.Pointer(cIfName))
	dev_fd := C.create_interface(cIfName, cIfType, C.int(user), C.int(group))
	if(int(dev_fd) < 0) {
		cErrStr := C.strerror(dev_fd)
		defer C.free(unsafe.Pointer(cErrStr))
		errStr := C.GoString(cErrStr)
		return "", errors.New("Could not create " + interfaceType + " device: " + errStr)
	}
	// Let go of the interface so that the user can use it.
	if int(C.close(dev_fd)) != 0 {
		log.Fatal("FATAL ERROR: Could not close interface file for " + interfaceName)
	}
	ifName := C.GoString(cIfName)

	return ifName, nil
}

// Destroys an interface
func deleteInterface(interfaceType, interfaceName string) error {
	return nil
}

// Change the state of the device to UP
func upInterface(interfaceName string) error {
	return nil
}

// Change the state of the device to DOWN
func downInterface(interfaceName string) error {
	return nil
}