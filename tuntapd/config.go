package tuntapd

import (
	"bufio"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

var (
	configMapMutex = sync.RWMutex{}
	configMap = map[string]string{}

	defaultconfigMap = map[string]string{
		"dir":             "/var/run/tuntapd",
		"group":           "nogroup",
		"allow-tun":       "false",
		"allow-tap":       "false",
		"allow-setip":     "false",
		"tunprefix":       "ttdtun",
		"tapprefix":       "ttdtap",
		"ip4":             "none",
		"ip6":             "none",
	}
)

const (
	mainSocketName = "main-control"
)


// sets the config file path. can only be done once, if called more than once it will panic and kill tuntapd.
func SetConfigFilePath(configFilePath string) {
	if _,present := defaultconfigMap["configFilePath"]; present {
		log.Fatal("Internal error. Tried to update path of config file, which should be set only once!")
	}
	defaultconfigMap["configFilePath"] = configFilePath
}


// getter for all values in the config map
func GetConfigOption(optionName string) string {
	configMapMutex.RLock()
	value := configMap[optionName]
	configMapMutex.RUnlock()
	return value
}


// reloads the config file
func reloadConfigFile() {
	configMapMutex.Lock()
	readConfigFile()
	configMapMutex.Unlock()
}


// reads the config file and updates the configMap
func readConfigFile() {
	// reads the config file line by line and sets various variables
	configFilePath := defaultconfigMap["configFilePath"]

	log.Printf("Reading config file %s\n", configFilePath)

	fileBuffer, e := ioutil.ReadFile(configFilePath)
	if e != nil {
		log.Printf("Fatal. Could not read config file. Error: %s", e.Error())
		os.Exit(-1)
	}

	// copy default config. later we replace the default values with values from the config file.
	newConfigMap := make(map[string]string)
	for k, v := range defaultconfigMap {
		configMap[k] = v
	}

	log.Println("Config:")
	lineScanner := bufio.NewScanner(strings.NewReader(string(fileBuffer)))
	for lineNum := 1; lineScanner.Scan(); lineNum++ {
		line := strings.TrimSpace(lineScanner.Text())
		if len(line) == 0 || (len(line) > 0 && line[0] == '#') {
			// ignore empty lines and lines that begin with #
			continue
		}
		log.Println("    " + line)

		var keyword string
		keyVal := strings.SplitN(line, " ", 2)
		if len(keyVal) == 2 {
			keyword = strings.ToLower(strings.TrimSpace(keyVal[0]))
		} else {
			log.Printf("Error on line %d.", lineNum)
			os.Exit(-1)
		}

		value := strings.TrimSpace(keyVal[1])
		switch(keyword) {
		case "dir", "group", "tunprefix", "tapprefix" :
			delete(newConfigMap, keyword)
			newConfigMap[keyword] = value
		case "allow" :
			if value == "tun" {
				delete(configMap, "allow-tun")
				newConfigMap["allow-tun"] = "true"
			} else if value == "tap" {
				delete(configMap, "allow-tap")
				newConfigMap["allow-tap"] = "true"
			} else if value == "setip" {
				delete(configMap, "allow-setip")
				newConfigMap["allow-setip"] = "true"
			} else {
				log.Printf("Error on line %d.", lineNum)
				os.Exit(-1)
			}
		case "networks" :
			delete(newConfigMap, keyword)
			subnets := strings.SplitN(value, ",", 20)
			for _,subnet := range subnets {
				subnet := strings.TrimSpace(subnet)
				if subnet == "none" {
					subnets = []string{"none"}
					break
				} else if subnet == "unique-global-lan" {
					// thats OK
					continue
				} else {
					if _, _, err := net.ParseCIDR(subnet); err != nil {
						log.Printf("Error on line %d. %s", lineNum, err.Error())
						os.Exit(-1)
					}
				}
			}
		default : 
			log.Printf("Error on line %d. No such keyword (%s).", lineNum, keyword)
		}
	}
	log.Println("End of config.")
	configMap = newConfigMap
}


