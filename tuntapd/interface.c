/*

C functions for creating and deleting virtual interfaces.

Some of the code here is more or less a copy of the code presented in the
kernel documentation:
  * Documentation/networking/tuntap.txt
  * https://www.kernel.org/doc/Documentation/networking/tuntap.txt

It might be a good idea to read the above documentation before you change
anything here.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h> 
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>

#include <stdio.h>

/*
   Creates an interface
   Then makes the interface usable for other users than root

   This function needs to be run as root, or a user with the CAP_NET_ADMIN
   capability.

   If dev_name is "" the kernel will just allocate a random name for you.
   devtype NEEDS be set to either IFF_TUN|IFF_NO_PI or IFF_TAP|IFF_NO_PI
   uid is the user id that will end up owning the device
*/
int create_interface(char* dev_name, int devtype, int uid, int gid)
{
	int dev_fd, err;

	if( (dev_fd = make_interface(dev_name, devtype)) < 0 ){
		return dev_fd; // error
	}
	if( (err = set_interface_owner(dev_fd, uid)) < 0 ){
		return err;
	}
	if( (err = set_interface_group(dev_fd, gid)) < 0 ){
		return err;
	}
	if( (err = set_persistance(dev_fd, 1)) < 0 ){
		return err;
	}
	return dev_fd;
}

// Removes an interface
int delete_interface(char* name) {
	printf("Hello C-world!\n");
}

// puts an interface UP
int interface_up(char* name) {
	int fd = connect_netlink();
	int sequence_number = 0;

	struct nlmsghdr *nh;    /* The nlmsghdr with payload to send. */
	struct sockaddr_nl sa;
	struct iovec iov = { nh, nh->nlmsg_len };
	struct msghdr msg;

	msg = { &sa, sizeof(sa), &iov, 1, NULL, 0, 0 };
	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;
	nh->nlmsg_pid = 0;
	nh->nlmsg_seq = ++sequence_number;
	/* Request an ack from kernel by setting NLM_F_ACK. */
	nh->nlmsg_flags |= NLM_F_ACK;

	sendmsg(fd, &msg, 0);
}

// puts an interface DOWN
int interface_down(char* name) {

}





// creates an inpersistant interface device
int make_interface(char* dev_name, int devtype) {
	struct ifreq ifr;
	int fd, err;

	if( (fd = open("/dev/net/tun", O_RDWR)) < 0 )
		return fd;

	memset(&ifr, 0, sizeof(ifr));

	ifr.ifr_flags = devtype;
	if( *dev_name )
		strncpy(ifr.ifr_name, dev_name, IFNAMSIZ);

	if( (err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0 ){
		close(fd);
		return err;
	}

	strcpy(dev_name, ifr.ifr_name);

	return fd;
}

/*
    Makes an tun/tap device persistance. A device needs to be persistant if we
    are to give it away to another process. A non-persistant device will
    cease to exist when we close the file descriptor pointing to it.

    persist: 1 for setting persistance,
             0 for unsetting it
*/
int set_persistance(int dev_fd, int persist) {
	int err;

	if( (err = ioctl(dev_fd, TUNSETPERSIST, persist)) < 0 ){
		return err;
	}
	return 0;
}

// Sets the owner of an interface
int set_interface_owner(int dev_fd, int owner) {
	int err;

	if( (err = ioctl(dev_fd, TUNSETOWNER, owner)) < 0 ){
		return err;
	}
	return 0;
}

// Sets the group of an interface
int set_interface_group(int dev_fd, int group) {
	int err;

	if( (err = ioctl(dev_fd, TUNSETGROUP, group)) < 0 ){
		return err;
	}
	return 0;
}

/*
   Returns a file descriptor for a socket bound to NETLINK, configured so 
   that it receives updates on links going up/down, et.c. as well as updates
   about interfaces adding/removing IPv4/IPv6 addresses 

   Returns <0 if failure, or the file descriptor to the netlink socket.

   See man 7 netlink for more info.
*/
int connect_netlink() {
	struct sockaddr_nl sa;
	int err;

	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;

	// listen on these messages
	sa.nl_groups = RTMGRP_LINK            // links going UP/DOWN
                 | RTMGRP_IPV4_IFADDR     // add/del IPv4 addresses
                 | RTMGRP_IPV6_IFADDR;    // add/del IPv6 addresses

	if( (fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0 ){
		return fd;
	}
	if( !(err = bind(fd, (struct sockaddr *) &sa, sizeof(sa))) ){
		return err;
	}

	return fd;
}