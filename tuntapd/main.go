package tuntapd

import (
	"path/filepath"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

// starts the program
func StartTuntapd() {
	/*
		gets config options by calling readConfigFile()
		checks if the server is already running
		create 
			($DIR) = directory to keep runtime UNIX sockets and pid file in
			($DIR)/pid = pid file
			($DIR)/users = directory for containing sockets connected to virtual interfaces
			($DIR)/main-control = main control socket

		creates a goroutine for listening on the main-control socket
		main goroutine listens on signals
	*/

	readConfigFile()
	shouldStart, eraseDir, statusStr := checkIfIShouldStart()
	if statusStr != "" {
		log.Println(statusStr)
	}
	if shouldStart {
		workDir := configMap["dir"]
		pidFileName := filepath.Join(workDir, "pid")
		usersDirPath := filepath.Join(workDir, "users")
		pathToMainSocket := filepath.Join(workDir, mainSocketName)

		if eraseDir {
			// erases old directory and everything in it
			log.Println("Erasing old working directory " + workDir)
			if err := os.RemoveAll(workDir); err != nil {
				log.Fatal(err)
			}
		}
		// creates ($DIR)
		log.Println("Creating and populating directory " + workDir)
		if err := os.Mkdir(workDir, 0755); err != nil {
			log.Fatal(err)
		}
		// create ($DIR)/users
		if err := os.Mkdir(usersDirPath, 0755); err != nil {
			log.Fatal(err)
		}
		myPidStr := strconv.Itoa(os.Getpid())
		if err := ioutil.WriteFile(pidFileName, []byte(myPidStr), 0644); err != nil {
			log.Println("Could not create pid file.")
			os.Exit(-1)
		}
		go listenOnMainSocket(pathToMainSocket)
		signalCatcher()
	}

}

// Returns true if the daemon should continue to start
func checkIfIShouldStart() (shouldStart bool, eraseDirectory bool, statusString string) {

	// check if our working directory exist
	fileInfo, err := os.Lstat(configMap["dir"])
	if err != nil {
		if os.IsNotExist(err) {
			// it does not exist and that means that we should start
			return true, false, ""
		} else {
			return false, false, "Can not lstat(2) directory."
		}
	}
	if ! fileInfo.Mode().IsDir() {
		// directory exist but its not a directory (!!) bail out
		statusString = configMap["dir"] + " is not a directory."
		return false, false, statusString
	}

	pathToPidFile := filepath.Join(configMap["dir"], "pid")
	fileInfo, err = os.Lstat(pathToPidFile)
	if err != nil {
		if os.IsNotExist(err) {
			// it does not exist and that means that we should start
			return true, true, "No server is running but directory is present."
		} else {
			log.Fatal(err)
		}
	}
	if ! fileInfo.Mode().IsRegular() {
		return false, false, "Pid file is not a regular file."
	} else {
		// read the pid file
		bytes, err := ioutil.ReadFile(pathToPidFile)
		if err != nil {
			return false, false, "Can not read pid file."
		}
		pid, err := strconv.Atoi( string(bytes) )
		if err != nil {
			return false, false, "I do not understand the content of the pid file."
		}
		_,err = os.FindProcess(pid)
		if err != nil {
			// pid file points at a process that does not exist
			// we are interpreting this as the old server died abruptly
			return true, true, "Pid file points to process that does not exist. Assuming tuntapd died horrible."
		} else {
			// the server is up and running, do not start
			return false, false, "Pid file suggest tuntapd is alive and healthy."
		}
	}

}

func terminateTuntapd() {
	/*
		close ($DIR)/main-control
		close all open user-created sockets
		deletes all created virtual interfaces (tun and tap devices)
		deletes everything in ($DIR) and the $DIR itself
		terminates
	*/
	os.Exit(0)
}



func signalCatcher() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT)

	// Block until a signal is received.
	for {
		s := <-c
		switch s {
		case syscall.SIGHUP :
			log.Println("Got SIGHUP. Reloading config file.")
			reloadConfigFile()
		case syscall.SIGINT :
			log.Println("Got SIGINT. Terminates.")
			terminateTuntapd()
		}
	}
}