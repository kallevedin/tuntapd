package tuntapd

import (
	"net"
	"log"
	"strings"
	"syscall"
	"os"
)

const (
	maxLineLength = 1024		// maximum length of incomming commands in bytes
)


func listenOnMainSocket(pathToMainSocket string) {
	unixAddr, err := net.ResolveUnixAddr("unix", pathToMainSocket)
	if err != nil {
		log.Fatal(pathToMainSocket + " not suitable for holding a UNIX socket")
	}
	listener, err := net.ListenUnix("unix", unixAddr)
	if err != nil {
		log.Println("Could not open and listen on main control socket %s", pathToMainSocket)
		log.Fatal(err)
	}
	// allow anyone to connect
	os.Chmod(pathToMainSocket, 0777)
	for {
		clientConn, err := listener.AcceptUnix()
		if err != nil {
			log.Fatal("Could not accept incomming connection to main socket.")
		}
		go handleMainClient(clientConn)
	}
}



func handleMainClient(conn *net.UnixConn) {
	defer conn.Close()

	readbuf := make([]byte, maxLineLength)

	for {
		read, err := conn.Read(readbuf)
		if err != nil {
			conn.Write([]byte("ERROR could not read\n"))
			return
		}
		line := strings.TrimSpace(string(readbuf[0:read]))
		keyVal := strings.SplitN(line, " ", 2)

		switch keyVal[0] {

		case "hello" :
			conn.Write([]byte("OK\n"))

		case "getname" :
			// TODO: Return a name and add it to the list of names that the uid owns

		case "add" :
			// syntax: add {tun|tap} NAME [unix]

			// check if user is allowed to create interfaces
			ucred, err := getClientCredentials(conn)
			if err != nil {
				log.Println("Could not determine user credentials of connected client!")
				log.Fatal(err)
			}
			// TODO: check if the user is allowed to create interfaces...

			// parse message
			args := strings.SplitN(keyVal[1], " ", 3)
			var unix bool
			if len(args) == 2 {
				unix = false
			} else if len(args) == 3 && args[2] == "unix" {
				unix = true
			} else {
				conn.Write([]byte("ERROR syntax\n"))
				return
			}
			interfaceType := args[0]
			if interfaceType != "tun" && interfaceType != "tap" {
				conn.Write([]byte("ERROR only supports tun or tap interfaces\n"))
				return
			}
			interfaceName := args[1]
			log.Printf("Creating %s interface %s for PID: %d, user %d:%d. unix = %t", interfaceType, interfaceName, ucred.Pid, ucred.Uid, ucred.Gid, unix)
			realInterfaceName, err := createInterface(interfaceType, interfaceName, ucred.Uid, ucred.Gid)
			if err != nil {
				log.Println("Unable to create " + interfaceName + ": " + err.Error())
				conn.Write([]byte("ERROR tuntapd failure\n"))
			}
			conn.Write([]byte("OK " + realInterfaceName + "\n"))
			// possibly attach it to a UNIX socket

		case "del" :
			// has to implement building interfaces first...

		case "up" :
			// up NAME
			// turn interface online somehow...

		case "down" :
			// down NAME
			// pull the plug for the interface...

		case "setip" :
			// unknown how this should work... os.exec??

		case "delip" :
			// delip NAME

		default :
			conn.Write([]byte("ERROR unknown command\n"))
			return
		}
	}
}

// Returns the PID, UID, GID of connected client. (Needed for authentication.)
func getClientCredentials(conn *net.UnixConn) (*syscall.Ucred, error) {
    file, err := conn.File()
    if err != nil {
        return nil, err
    }
    defer file.Close()
    return syscall.GetsockoptUcred(int(file.Fd()), syscall.SOL_SOCKET, syscall.SO_PEERCRED)
}