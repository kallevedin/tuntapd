package main

import (
	"bitbucket.org/kallevedin/tuntapd/tuntapd"
	"flag"
	"runtime"
)



// The program starts here.
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	configFilePath := flag.String("conf", "/etc/tuntapd/conf", "specifies which config file to use")
	flag.Parse()

	tuntapd.SetConfigFilePath(*configFilePath)
	tuntapd.StartTuntapd()
}



